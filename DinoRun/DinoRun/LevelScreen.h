#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Obstacle.h"
#include "AssetManager.h"

class Game;


class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

	bool gameOver;

private:
	Game* gamePointer;
	Player playerInstance;
	std::vector<Obstacle*> obstacles;
	sf::View camera;
	sf::Font gameFont;
	sf::Text scoreText;

	float obsticleGap;
	float farthestObsticle;
	float obsticleBuffer;
	int score;

	void AddObstacle();
};

