#include "Obstacle.h"
#include "AssetManager.h"

Obstacle::Obstacle()
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/cactus.png"))
	, speed(300)
{

}

Obstacle::Obstacle(sf::Texture& newTexture)
	: SpriteObject(newTexture)
{

}

void Obstacle::Update(sf::Time frameTime)
{
	//Record original position
	sf::Vector2f originalPosition = sprite.getPosition();

	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition();
	newPosition.x -= speed * frameTime.asSeconds();

	// Move the platform to the new position
	sprite.setPosition(newPosition);
}

void Obstacle::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}