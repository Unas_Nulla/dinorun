#pragma once
#include "SpriteObject.h"
class Obstacle :
    public SpriteObject
{
public:
    // Constructors / Destructors
    Obstacle();
    Obstacle(sf::Texture& newTexture);
    virtual void Update(sf::Time frameTime);
    void SetPosition(sf::Vector2f newPosition);

    

private:
    float speed;
};

