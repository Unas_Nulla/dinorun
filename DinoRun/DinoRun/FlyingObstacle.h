#pragma once
#include "Obstacle.h"
class FlyingObstacle :
    public Obstacle
{
public:
    // Constructors
    FlyingObstacle();
    void Update(sf::Time frameTime);


private:
    float speed;
    
};

