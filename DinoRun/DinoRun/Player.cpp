#include "Player.h"
#include "AssetManager.h"
#include "LevelScreen.h"
#include "Game.h"

Player::Player(sf::Vector2u screenSize)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/DinoSprite.png"), 100, 80, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, gravity(800.0f)
	, previousPosition()
	, collide(false)
	, isCrouching(false)
{
	AddClip("Run", 0, 1);
	PlayClip("Run", true);

	AddClip("Crouch", 2, 3);
	AddClip("Jump", 4, 5);
	AddClip("Dead", 6, 6);

	// position the player at the centre of the screen	
	spawnPoint.x = (((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f) - 200;
	spawnPoint.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(spawnPoint);
}

bool Player::GetCollide()
{
	return collide;
}

void Player::Input()
{
	 isCrouching = false;

	if (sprite.getPosition().y >= spawnPoint.y)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			velocity.y = -500;
			PlayClip("Jump", false);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			PlayClip("Crouch", true);
			isCrouching = true;
		}
		else
		{
			PlayClip("Run", true);
		}
	}
	
}

void Player::Update(sf::Time frameTime)
{
	// Calculate the new velocity
	if (sprite.getPosition().y <= spawnPoint.y)
	{
		velocity.y = velocity.y + gravity * frameTime.asSeconds();
	}
	else
	{
		sprite.setPosition(previousPosition);
	}
	// Save old position
	previousPosition = sprite.getPosition();

	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	// Move the player to the new position
	sprite.setPosition(newPosition);
	AnimatingObject::Update(frameTime);
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	// If we are colliding with a Platform
	if (GetHitBox().intersects(otherHitbox))
	{
		PlayClip("Dead", false);
		collide = true;
	}
}

sf::FloatRect Player::GetHitBox()
{
	sf::FloatRect oldHitBox = SpriteObject::GetHitBox();

	sf::FloatRect newHitBox = oldHitBox;

		if (isCrouching)
		{
			newHitBox.height = newHitBox.height / 2;
			newHitBox.top += oldHitBox.height / 2;
		}
	return newHitBox;
}
