#include "LevelScreen.h"
#include "Game.h"
#include "FlyingObstacle.h"
#include <iostream>
#include <stdlib.h>

LevelScreen::LevelScreen(Game* newGamePointer)
	: playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, obstacles()
	, camera(newGamePointer->GetWindow().getDefaultView())
	, obsticleGap(80)
	, farthestObsticle(0)
	, obsticleBuffer(200)
	, gameOver(false)
	, gameFont(AssetManager::RequestFont("Assets/Fonts/enter-command.ttf"))
{
	// Create the starting obstacle
	Obstacle* obstacleInstance = new Obstacle();

	// Calculate center of the screen
	sf::Vector2f newPosition;
	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;

	// Calculate position of obstacle to be centered
	newPosition.x -= obstacleInstance->GetHitBox().width / 2;
	newPosition.y -= obstacleInstance->GetHitBox().height / 2;

	// Add to the y position to lower the obstacle a bit
	const float OBSTACLE_OFFSET = 500; // this can be adjusted as needed
	newPosition.x += OBSTACLE_OFFSET;
	
	// Set the new position of the obstacle
	obstacleInstance->SetPosition(newPosition);

	// Copy the starting obstacle into the obstacle collection
	obstacles.push_back(obstacleInstance);

	// Populate the rest of the obstacles
	farthestObsticle = newPosition.x;

	// Place obstacles until the highest one is outside the camera buffer zone
	float cameraRight = camera.getCenter().x + camera.getSize().x / 2.0f;

	while (farthestObsticle < cameraRight + obsticleBuffer)
	{
		AddObstacle();
	}
	score = 0;
	
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(camera.getCenter().x - 500, camera.getCenter().y - 500);
}


void LevelScreen::Input()
{
	if (!gameOver)
	{
		playerInstance.Input();
	}
}

void LevelScreen::Update(sf::Time frameTime)
{
	if (!gameOver)
	{
		float currentTime = frameTime.asSeconds();

		playerInstance.Update(frameTime);
		for (int i = 0; i < obstacles.size(); i++)
		{
			
			playerInstance.HandleSolidCollision(obstacles[i]->GetHitBox());

			if (playerInstance.GetCollide() == true)
			{
				gameOver = true;
			}
		}
		
		

		// Place a new obstacle if needed
		float cameraRight = camera.getCenter().x + camera.getSize().x / 2.0f;

		if (farthestObsticle > cameraRight + obsticleBuffer)
		{
			AddObstacle();
		}

		// If the bottom obstacle is off screen, remove it
		if (obstacles[0]->GetHitBox().left < camera.getCenter().x - camera.getSize().x / 2 - obstacles[0]->GetHitBox().width)
		{
			// Delete the actual obstacle (freeing memory)
			delete obstacles[0];

			// Erase the first element of the vector
			obstacles.erase(obstacles.begin());

			// Add a new obtacle
			AddObstacle();
		}

		// Update the obstacles
		for (int i = 0; i < obstacles.size(); ++i)
		{
			obstacles[i]->Update(frameTime);
		}

		//score
		
		scoreText.setString("Score: " + std::to_string(score));//score update
	}
}

void LevelScreen::DrawTo(sf::RenderTarget& target)
{
	// Update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();
	/*float playerCenterY = playerInstance.GetHitBox().top + playerInstance.GetHitBox().height / 2;
	if (playerCenterY < currentViewCenter.y)
	{
		camera.setCenter(currentViewCenter.x, playerCenterY);
	}*/

	// Set camera view
	target.setView(camera);

	// Draw content to screen
	playerInstance.DrawTo(target);
	for (int i = 0; i < obstacles.size(); i++)
	{
		obstacles[i]->DrawTo(target);
	}

	target.draw(scoreText);
	
	//Remove camera view
	target.setView(target.getDefaultView());
}


void LevelScreen::AddObstacle()
{
	// Play area calculations
	int SPAWN_AREA_WIDTH = 200;
	
	// Create new obstacle
	Obstacle* newObstacle = nullptr;

	// Choose the type of obstacle to create
	int choiceMin = 0;
	int choiceMax = 100;
	int choice = rand() % (choiceMax - choiceMin) + choiceMin;
	int chanceFlying = 50;

	sf::Vector2f newPosition;
	
	if (choice < chanceFlying)
	{
		newObstacle = new FlyingObstacle();
		newPosition.y = camera.getCenter().y - 100;
	}
	else
	{
		newObstacle = new Obstacle();
		newPosition.y = camera.getCenter().y - 50;
	}	

	newPosition.x = camera.getCenter().x + camera.getSize().x / 2 + SPAWN_AREA_WIDTH;

	// Set new obstacle position
	newObstacle->SetPosition(newPosition);
	
	// Add the new obstacle to the list
	obstacles.push_back(newObstacle);

	// Update farthest obstacle
	farthestObsticle = newPosition.x;

	score += 1;
}