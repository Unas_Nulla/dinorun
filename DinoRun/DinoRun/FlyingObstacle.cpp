#include "FlyingObstacle.h"
#include "AssetManager.h"

FlyingObstacle::FlyingObstacle()
	: Obstacle(AssetManager::RequestTexture("Assets/Graphics/flyer-1.png"))
	, speed(300)
{

}

void FlyingObstacle::Update(sf::Time frameTime)
{
	//Record original position
	sf::Vector2f originalPosition = sprite.getPosition();

	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition();
	newPosition.x -= speed * frameTime.asSeconds();
	
	// Move the platform to the new position
	sprite.setPosition(newPosition);	
}